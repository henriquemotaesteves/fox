plugins {
    kotlin("jvm") version "1.3.61"

    application
}

repositories {
    jcenter()
}

dependencies {
    implementation(kotlin("stdlib-jdk8"))
    implementation("io.ktor:ktor-client-cio:1.3.0")
    implementation("io.ktor:ktor-client-json:1.3.0")
    implementation("io.ktor:ktor-client-json-jvm:1.3.0")
    implementation("io.ktor:ktor-client-gson:1.3.0")
    implementation("org.eclipse.jgit:org.eclipse.jgit:5.6.1.202002131546-r")

    testImplementation("org.junit.jupiter:junit-jupiter:5.5.2")
    testImplementation("com.github.tomakehurst:wiremock-jre8:2.26.0")
}

application {
    mainClassName = "io.github.henriquemotaesteves.fox.MainKt"
}

tasks {
    withType<Test> {
        useJUnitPlatform()
    }
}
