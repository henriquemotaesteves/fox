package io.github.henriquemotaesteves.fox.domain.support

import io.github.henriquemotaesteves.fox.domain.Project
import org.junit.jupiter.api.Assertions.assertEquals

fun Project.assertEquals(other: Project) {
    assertEquals(this.name, other.name)
    assertEquals(this.repositoryURL, other.repositoryURL)
}