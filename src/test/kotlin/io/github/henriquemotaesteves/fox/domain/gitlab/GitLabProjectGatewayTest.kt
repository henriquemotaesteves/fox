package io.github.henriquemotaesteves.fox.domain.gitlab

import com.github.tomakehurst.wiremock.WireMockServer
import io.github.henriquemotaesteves.fox.domain.Project
import io.github.henriquemotaesteves.fox.domain.support.assertEquals
import io.ktor.util.KtorExperimentalAPI
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test

@KtorExperimentalAPI
class GitLabProjectGatewayTest {
    companion object {
        private val SERVER = WireMockServer()

        @BeforeAll
        @JvmStatic
        fun beforeAll() {
            SERVER.start()
        }

        @AfterAll
        @JvmStatic
        fun afterAll() {
            SERVER.stop()
        }
    }

    @Test
    fun `#projects returns all projects for the specified user`() {
        // Arrange
        val projectGateway = GitLabProjectGateway(SERVER.baseUrl(), "fox", "BDEKzEExdWyfi1sri7Vw")

        // Act
        val projects = projectGateway.projects()

        // Assert
        assertEquals(9, projects.size)

        projects[0].assertEquals(Project("project-9", "http://gitlab.example.com/fox/project-9.git"))
        projects[1].assertEquals(Project("project-8", "http://gitlab.example.com/fox/project-8.git"))
        projects[2].assertEquals(Project("project-7", "http://gitlab.example.com/fox/project-7.git"))
        projects[3].assertEquals(Project("project-6", "http://gitlab.example.com/fox/project-6.git"))
        projects[4].assertEquals(Project("project-5", "http://gitlab.example.com/fox/project-5.git"))
        projects[5].assertEquals(Project("project-4", "http://gitlab.example.com/fox/project-4.git"))
        projects[6].assertEquals(Project("project-3", "http://gitlab.example.com/fox/project-3.git"))
        projects[7].assertEquals(Project("project-2", "http://gitlab.example.com/fox/project-2.git"))
        projects[8].assertEquals(Project("project-1", "http://gitlab.example.com/fox/project-1.git"))
    }
}
