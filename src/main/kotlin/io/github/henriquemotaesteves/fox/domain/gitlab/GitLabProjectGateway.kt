package io.github.henriquemotaesteves.fox.domain.gitlab

import io.github.henriquemotaesteves.fox.domain.Project
import io.github.henriquemotaesteves.fox.domain.ProjectGateway
import io.ktor.client.HttpClient
import io.ktor.client.engine.cio.CIO
import io.ktor.client.features.json.GsonSerializer
import io.ktor.client.features.json.JsonFeature
import io.ktor.client.request.get
import io.ktor.client.request.header
import io.ktor.util.KtorExperimentalAPI
import kotlinx.coroutines.runBlocking

@KtorExperimentalAPI
class GitLabProjectGateway(private val url: String, private val user: String, private val token: String) : ProjectGateway {
    override fun projects(): List<Project> = runBlocking {
        val client = HttpClient(CIO) {
            install(JsonFeature) {
                serializer = GsonSerializer()
            }
        }

        client.use {
            val projects = client.get<List<GitLabProjectModel>>("$url/api/v4/users/$user/projects") {
                header("Private-Token", token)
            }

            projects.map { p -> Project(p.name, p.http_url_to_repo) }
        }
    }
}
