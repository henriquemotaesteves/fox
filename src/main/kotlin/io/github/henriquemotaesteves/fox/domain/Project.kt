package io.github.henriquemotaesteves.fox.domain

class Project(val name: String, val repositoryURL: String)
