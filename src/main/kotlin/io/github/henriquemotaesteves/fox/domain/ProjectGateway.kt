package io.github.henriquemotaesteves.fox.domain

interface ProjectGateway {
    fun projects(): List<Project>
}
