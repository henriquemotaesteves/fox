package io.github.henriquemotaesteves.fox.domain.gitlab

data class GitLabProjectModel(val name: String, val http_url_to_repo: String)